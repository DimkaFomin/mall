<%--
  Created by IntelliJ IDEA.
  User: Дмитрий
  Date: 23.04.2017
  Time: 12:30
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Prods</title>
</head>
<body>

<div><a href="<c:url value="/index.jsp"/>">Back</a></div>

    <table class="tg">
        <tr>
            <th width="120">id_prod</th>
            <th width="120">name</th>
            <th width="120">price</th>
            <th width="120">prod_desc</th>
            <th width="120">number</th>
            <th width="120">category</th>

        </tr>
            <div><a>OK!!!</a></div>
            <c:forEach items='${listProds}' var="prod">
                <tr>
                    <td>${prod.id_prod}</td>
                    <td>${prod.name}</td>
                    <td>${prod.price}</td>
                    <td>${prod.prod_desc}</td>
                    <td>${prod.number}</td>
                    <td>${prod.category}</td>
                </tr>
            </c:forEach>
    </table>


<div><a>OK!!!</a></div>
</body>
</html>
