<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Магазинчик</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style type="text/css">
        #header{
            background:darkred;
            width:100%;
            height:100px;
        }
        #list{
            background:oldlace;
            width:100%;
            height:300px;
        }
        #desc{
            background:oldlace;
            width:100%;
            height:300px;
        }
        #basket{
            background:darkred;
            width:100%;
            height:30px;
        }
    </style>
</head>

<body>
<div id="header">Category
    <select id="sel" onchange="Write()" name='choice'>
        <option value="Food">Food</option>
        <option value="Clothes">Clothes</option>
        <option value="Sport">Sport</option>
    </select>
</div>
<div><a href="<c:url value="/prods"/>">Click</a></div>

<div id="desc">Description</div>
<div id="basket">You bought <button>More</button>
</div>

</body>

</html>
