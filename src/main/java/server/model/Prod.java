package server.model;

import com.sun.org.apache.xpath.internal.operations.String;

import javax.persistence.*;

@Entity
@Table(name = "product")


public class Prod {
    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }


    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }


    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public char getProd_desc() {
        return prod_desc;
    }

    public void setProd_desc(char prod_desc) {
        this.prod_desc = prod_desc;
    }

    public char getCategory() {
        return category;
    }

    public void setCategory(char category) {
        this.category = category;
    }

    @Id
    @Column(name = "id_prod")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_prod;

    @Column(name = "name")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private char name;

    @Column(name = "price")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer price;

    @Column(name = "prod_desc")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private char prod_desc;

    @Column(name = "number")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer number;

    @Column(name = "category")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private char category;

    @Override
    public java.lang.String toString() {
        return "Prod{" +
                "id_prod=" + id_prod +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", prod_desc='" + prod_desc + '\'' +
                ", number='" + number + '\'' +
                ", category=" + category +
                '}';
    }
}
