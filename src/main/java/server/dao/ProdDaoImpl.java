package server.dao;


import server.model.Prod;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

public class ProdDaoImpl implements ProdDao {

    private static final Logger logger = LoggerFactory.getLogger(ProdDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<Prod> listProds() {
        Session session = this.sessionFactory.getCurrentSession();

        List<Prod> prodList = session.createQuery("from Prod").list();

        for(Prod prod : prodList){
            logger.info("Prod list: " + prod);
        }
        return prodList;
    }


}
