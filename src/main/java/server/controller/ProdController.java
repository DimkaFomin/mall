package server.controller;

import server.model.Prod;
import server.service.ProdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
/**
 * Created by Дмитрий on 21.04.2017.
 */
public class ProdController {

    private ProdService prodService;

    @Autowired(required = true)
    @Qualifier(value = "ProdService")
    public void setCardService(ProdService prodService) {
        this.prodService = prodService;
    }

    @RequestMapping(value = "/prods", method = RequestMethod.GET)
    public String listProds(Model model) {
        model.addAttribute("prod", new Prod());
        model.addAttribute("listProds", this.prodService.listProds());
        return "prods";
    }

}
