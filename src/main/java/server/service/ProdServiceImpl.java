package server.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.dao.ProdDao;
import server.model.Prod;

import java.util.List;

@Service
public class ProdServiceImpl implements ProdService {
    private ProdDao prodDao;

    public void setProdDao(ProdDao prodDao) {
        this.prodDao = prodDao;
    }

    @Override
    @Transactional
    public List<Prod> listProds() {
        return this.prodDao.listProds();
    }
}
