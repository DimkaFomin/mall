package server.service;

import server.model.Prod;

import java.util.List;

public interface ProdService {

    public List<Prod> listProds();
}
